# bda

Código desenvolvido para a disciplina de Banco de Dados Avançados - BDA.

**OBS:** Como foi desenvolvido em sala de aula, nem todas as funcionalidade foram concluídas ou implementadas.


## Infos
 * Made in OS: Windows 7
 * Tested in OS: Windows 7
 * Language Used: PHP
 * License: BSD 3-Clause License


## How To
A utilização é feita somente via linha de comando:

    $ php sgbd.php

Ao executar, é liberado um "terminal" para entrar com queries SQL.


## Contributors
 * Fernando A. Damião - [GitHub](https://github.com/fadamiao) - [Bitbucket](https://bitbucket.org/fadamiao)


 ## Referencies
 * [replace/deletes a specific line in a text file - PHP Coding Help - PHP Freaks](http://forums.phpfreaks.com/topic/230189-replacedeletes-a-specific-line-in-a-text-file/#entry1185698)
 * [php explode function for only first white space - Stack Overflow](http://stackoverflow.com/a/9402295)
 * [Remove last comma of string in PHP | Jon Todd](http://www.jontodd.com/2006/09/11/remove-last-comma-of-string-in-php/comment-page-1/#comment-3796)
 * [regex - php Replacing multiple spaces with a single space - Stack Overflow](http://stackoverflow.com/a/2368546)
