<?php
// Author: Fernando A. Damião <me@fadamiao.com>
// Created At: 2013-05-15 19:10
// Last Update: 2013-06-12 20:15
// License: BSD 3-Clause License

ini_set('display_errors', 0);
include('lib/core.php');
$sgbd = new SGBD();
$input = null;

echo "----> Welcome to PHPSGBD {$sgbd->version} <----" . PHP_EOL;
while ($input != 'exit') {
    echo "> ";
    $input = trim(fgets(STDIN));

    $sgbd->query($input);
}
