<?php
// Author: Fernando A. Damião <me@fadamiao.com>
// Created At: 2013-05-15 19:10
// Last Update: 2013-06-26 17:32
// License: BSD 3-Clause License

class SGBD {
    private $filePath;
    private $tableFile;
    private $database;
    public $version;

    public function SGBD() {
        $this->filePath = dirname(__FILE__);
        $this->tableFile = $this->filePath . '/../data/databases';
        $this->version = '0.4.1';
    }

    public function query($query) {
        if (empty($query)) {
            return ' ';
        } else if ($query == 'exit') {
            echo 'Bye!';
            exit;
        }

        $command = explode(' ', $query);
        if ($command[0] == 'use') {
            $this->useDatabase($command[1]);
        } else if ($command[0] == 'insert') {
            $this->insertData($query);
        } else if ($command[0] == 'select') {
            $this->selectData($query);
        } else if ($command[0] == 'delete') {
            $this->deleteData($query);
        } else if ($command[0] == 'show' && $command[1] == 'databases') {
            $this->showDatabases();
        } else if ($command[0] == 'create' && count($command) > 2) {
            if ($command[1] == 'database') {
                $this->createDatabase($command[2]);
            } else if ($command[1] == 'table') {
                $this->createTable($query);
            }
        } else if ($command[0] == 'drop' && count($command) == 3) {
            if ($command[1] == 'database') {
                $this->dropDatabase($command[2]);
            } else if($command[1] == 'table') {
                $this->dropTable($command[2]);
            }
        } else {
            echo 'Error: Invalid command!' . PHP_EOL;
        }
    }

    private function showDatabases() {
        $bases = file($this->tableFile, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
        if (count($bases) > 0) {
            foreach ($bases as $base) {
                echo " |-> {$base}" . PHP_EOL;
            }
        } else {
            echo 'Info: No databases found!' . PHP_EOL;
        }
    }

    private function createDatabase($base) {
        if (!empty($base) && isset($base)) {
            if ($this->databaseExists($base)) {
                echo "Error: Database '{$base}' already exists!" . PHP_EOL;
            } else {        
                file_put_contents($this->tableFile, $base . PHP_EOL, FILE_APPEND);
                mkdir("{$this->filePath}/../data/{$base}");
                echo "Database '{$base}' created!" . PHP_EOL;
            }
        } else {
            echo 'Error: Please insert a database name!' . PHP_EOL;
        }
    }

    private function useDatabase($base) {
        if ($this->databaseExists($base)) {
            $this->database = $base;
        } else {
            echo "Error: Database '{$base}' not exists!" . PHP_EOL;
        } 
    }

    private function createTable($query) {
        if (empty($this->database)) {
            echo "Error: Select a database first!" . PHP_EOL;
            return false;
        }

        if ($this->databaseExists($this->database)) {
            $query = explode('(', $query, 2);
            $command = explode(' ', trim($query[0]));

            if (empty($command[2]) && !isset($command[2])) {
                echo 'Error: please inform a table name' . PHP_EOL;
                return false;
            }

            if ($this->tableExists($command[2])) {
                echo "Error: Table '{$command[2]}' already exists!" . PHP_EOL;
            } else {
                $table = $command[2];
                $fields = substr($query[1], 0, -1);                
                $fields = explode(', ', $fields);
                $fieldBuff = array();
                foreach ($fields as $field) {
                    $field = explode(' ', $field);
                    $fieldBuff[] = $field[0];
                }
                $fields = implode(';', $fieldBuff);

                file_put_contents("{$this->filePath}/../data/{$this->database}/tables", $table . PHP_EOL, FILE_APPEND);
                file_put_contents("{$this->filePath}/../data/{$this->database}/{$table}", $fields . PHP_EOL, FILE_APPEND);
                echo "Table '{$table}' created on database '{$this->database}'!" . PHP_EOL;
            }
        } else {
            echo "Error: Database '{$this->database}' not exists!" . PHP_EOL;
        }
    }

    private function insertData($query) {
        if (empty($this->database)) {
            echo "Error: Select a database first!" . PHP_EOL;
            return false;
        }

        if ($this->databaseExists($this->database)) {
            $query = explode('(', $query, 2);
            $command = explode(' ', trim($query[0]));

            if (empty($command[2]) && !isset($command[2])) {
                echo 'Error: please inform a table name' . PHP_EOL;
                return false;
            }

            if ($this->tableExists($command[2])) {
                $table = $command[2];
                $fields = substr($query[1], 0, -1);                
                $fields = explode(', ', $fields);
                $fieldBuff = array();
                foreach ($fields as $field) {
                    $field = explode(' ', $field);
                    $fieldBuff[] = $field[0];
                }
                $fields = implode(';', $fieldBuff);

                file_put_contents("{$this->filePath}/../data/{$this->database}/{$table}", $fields . PHP_EOL, FILE_APPEND);
            } else {
                echo "Error: Table '{$command[2]}' not exists!" . PHP_EOL;
            }
        } else {
            echo "Error: Database '{$this->database}' not exists!" . PHP_EOL;
        }
    }

    private function selectData($query) {
        if (empty($this->database)) {
            echo "Error: Select a database first!" . PHP_EOL;
            return false;
        }

        if ($this->databaseExists($this->database)) {
            $query = explode(' from ', $query, 2);
            $command = explode(' where ', trim($query[1]));
            $table = $command[0];

            if (empty($table) && !isset($table)) {
                echo 'Error: please inform a table name' . PHP_EOL;
                return false;
            }

            if ($this->tableExists($table)) {
                $data = str_replace('=', '', $command[1]);
                $data = preg_replace('!\s+!', ' ', $data);
                $data = explode(' ', $data);
                $param = $data[0];
                $value = $data[1];

                $lines = file("{$this->filePath}/../data/{$this->database}/{$table}", FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
                $fields = explode(';', $lines[0]);
                array_shift($lines);
                $lineSize = count($lines);
                for ($i = 0; $i < $lineSize; $i++) {
                    $line = explode(';', $lines[$i]);
                    $columnSize = count($line);
                    for ($j = 0; $j < $columnSize; $j++) {
                        if ($fields[$j] == $param) {
                            if ($value == $line[$j]) {
                                echo implode(' | ', $fields) . PHP_EOL;
                                echo str_replace(';', ' | ', $lines[$i]) . PHP_EOL;
                                return true;
                            }
                        }
                    }
                }
            } else {
                echo "Error: Table '{$table}' not exists!" . PHP_EOL;
            }
        } else {
            echo "Error: Database '{$this->database}' not exists!" . PHP_EOL;
        }
    }

    private function deleteData($query) {
        if (empty($this->database)) {
            echo "Error: Select a database first!" . PHP_EOL;
            return false;
        }

        if ($this->databaseExists($this->database)) {
            $query = explode(' from ', $query, 2);
            $command = explode(' where ', trim($query[1]));
            $table = $command[0];

            if (empty($table) && !isset($table)) {
                echo 'Error: please inform a table name' . PHP_EOL;
                return false;
            }

            if ($this->tableExists($table)) {
                $data = str_replace('=', '', $command[1]);
                $data = preg_replace('!\s+!', ' ', $data);
                $data = explode(' ', $data);
                $param = $data[0];
                $value = $data[1];

                $lines = file("{$this->filePath}/../data/{$this->database}/{$table}", FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
                $fields = explode(';', $lines[0]);
                array_shift($lines);
                $lineSize = count($lines);
                for ($i = 0; $i < $lineSize; $i++) {
                    $line = explode(';', $lines[$i]);
                    $columnSize = count($line);
                    for ($j = 0; $j < $columnSize; $j++) {
                        if ($fields[$j] == $param) {
                            if ($value == $line[$j]) {
                                unset($lines[$i]);
                            }
                        }
                    }
                }

                $output = array_merge((array) implode(';', $fields), $lines);
                file_put_contents("{$this->filePath}/../data/{$this->database}/{$table}", implode(PHP_EOL, $output) . PHP_EOL);
            } else {
                echo "Error: Table '{$table}' not exists!" . PHP_EOL;
            }
        } else {
            echo "Error: Database '{$this->database}' not exists!" . PHP_EOL;
        }
    }

    private function dropTable($table) {
        if (empty($this->database)) {
            echo "Error: Select a database first!" . PHP_EOL;
            return false;
        }

        if ($this->databaseExists($this->database)) {
            if ($this->tableExists($table)) {
                $databasePath = "{$this->filePath}/../data/{$this->database}";
                unlink("{$databasePath}/{$table}");
                file_put_contents("{$databasePath}/tables", trim(str_replace($table, '', file_get_contents("{$databasePath}/tables"))) . PHP_EOL);
                echo "Table '{$table}' excluded!" . PHP_EOL;
            }
        } else {
            echo "Error: Database '{$this->database}' not exists!" . PHP_EOL;
        }          
    }

    private function tableExists($tbl) {
        if (empty($this->database)) {
            echo "Error: Select a database first!" . PHP_EOL;
            return false;
        }

        if ($this->databaseExists($this->database)) {
            $tableFile = "{$this->filePath}/../data/{$this->database}/tables";
            if (!file_exists($tableFile)) {
                return false;
            }

            $tables = file($tableFile, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
            foreach ($tables as $table) {
                if ($table == $tbl) {
                    return true;
                }
                return false;
            }
        } else {
            echo "Error: Database '{$this->database}' not exists!" . PHP_EOL;
            return false;
        }
    }

    private function dropDatabase($base) {
        if ($this->databaseExists($base)) {
            $files = glob("{$this->filePath}/../data/{$base}/*");
            foreach ($files as $file) {
                unlink($file);
            }
            rmdir("{$this->filePath}/../data/{$base}");
            file_put_contents($this->tableFile, trim(str_replace($base, '', file_get_contents($this->tableFile))) . PHP_EOL);
            echo "Database '{$base}' excluded!" . PHP_EOL;
        } else {
            echo "Error: Database '{$base}' not exists!" . PHP_EOL;
        }
    }

    private function databaseExists($base) {
        $databases = file($this->tableFile, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
        foreach ($databases as $database) {
            if ($database == $base) {
                return true;
            }
        }
        return false;
    }
}
